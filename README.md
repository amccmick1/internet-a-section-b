# Internet A - Section B

[Live web application demo](http://54.68.21.95/AIDA/SectionB/c3341430/) built on responsive framework [Skel](http://getskel.com/). The site is a html content and asset clone of [Section A](http://54.68.21.95/AIDA/SectionA/c3341430/) but has been developed and modified to form an MVC application. This demo has been produced as supporting evidence for a third-year assignment on a BSC(Hons) Computing degree.

## Setup

For detailed hosting server setup through Amazon Web Services (AWS) Elastic Compute 2 (EC2) see the Section A [readme](https://bitbucket.org/amccmick1/internet-a-section-a). The only additional step was to change the apache2.conf (config) file located in /etc/apache2 from "AllowOverride None" to AllowOverride All" between the <Directory /var/www> and </Directory> tags to allow the .htaccess file in the root directory of the site to remove 'index.php' from the uri. The change only takes effect after restarting apache with the ```service apache2 restart``` command.

Optional: As a personal preference I change the password for the root user. This causes a problem when accessing http://localhost/phpMyAdmin whilst developing through XAMPP. To access phpmyadmin with your root password you need to use the Xampp Control Panel and open config.inc.php file in Apache Actions - Config. In this file you need to find $cfg['Servers'][$i]['password'] = ''; and enter your root password inbetween the singular quotation marks.

## Changelog 22-12-14 to 26-12-14

The following changes were made to the code offline:

* Add Pages controller 22-12-14
* Modified .htaccess to not interpret css||images||js uri arguments as controller names 22-12-14
* Added route to take values from index.php/$1 and process through pages/view/$1 method 22-12-14
* Added Header, Footer and Nav templates 22-12-14
* Added Base URL to config file 22-12-14
* Removed noscript tags 22-12-14
* Disabled skel css 22-12-14
* Added Page rendered in {elapsed_seconds} to footer 22-12-14
* Added Home view 22-12-14
* Added About view 22-12-14
* Added Contact view 22-12-14
* Removed route to pages/view in preparation for adding Contact controller 22-12-14
* Removed index.php remove rewrite 22-12-14
* Made links on Nav absolute with base_url 22-12-14
* Added Contact controller and configured to post data to itself 22-12-14
* Added Placeholders to Contact Form 23-12-14
* Switched off Section scroll transitions for development process 23-12-14
* Commented out Content Fade-All in for development process 23-12-14
* Added mail method to Contact controller 23-12-14
* Added Database credentials and applied autoload 23-12-14
* Added Register view, controller and model 23-12-14
* Added Account View 23-12-14
* Added Session to autoload 23-12-14
* Set Encryption key in config to 1 for development 23-12-14
* Moved git repo up one directory for hosting purposes 23-12-14
* Added Email address Session echo to Account page 23-12-14
* Added clause to Register controller which logs you in if you use correct email and password 23-12-14
* Added Nav bar dynamic account/register link 23-12-14
* Removed index.php from uri in .htaccess 23-12-14
* Added login and logout routes from root 23-12-14
* Added profiler information for developers email address login through a CI hook 23-12-14
* Added admin controller, view and nav link 24-12-14 
* Added create, view, delete methods to admin controller 24-12-14