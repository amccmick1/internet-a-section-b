<?php

	class Contact extends CI_Controller {
	
		function __construct() {
				
			parent::__construct();
						
			# Load Helper URL for Loading Views
			$this->load->helper('url');
			
			# Load Form helper for dynamic php form building
			$this->load->helper('form');
			
			# Include Form Validation Library
			$this->load->library('form_validation');
			
			# Add Validation Rules to Form
			$this->form_validation->set_rules('name','Name', 'strip_tags|required|trim|xss_clean');
			$this->form_validation->set_rules('email','Email', 'strip_tags|required|trim|valid_email|xss_clean');
			$this->form_validation->set_rules('message','Message', 'strip_tags|required|xss_clean');
		
		}
	
		public function form() {

			# Load Header Template
			$this->load->view('templates/header');
			
			# Load Nav Template
			$this->load->view('templates/nav');
			
			# Load Contact Form
			$this->load->view('contact');
			
			# Load Footer Template
			$this->load->view('templates/footer');
		}
		
		public function submit() {
			
			# Load Contact Form and Validate
			if ( $this->form_validation->run() == FALSE )
				$this->form();
			else {
			
				# Build Email from Post Data
				$this->load->library('email');
				$this->email->from($this->input->post('email'));
				$this->email->to('amccmick1@googlemail.com');
				$this->email->subject('AIDA - Section B MVC');
				$this->email->message($this->input->post('message'));
			
				# Load Header Template
				$this->load->view('templates/header');
				
				# Load Nav Template
				$this->load->view('templates/nav');
			
				# Send Email
				if( $this->email->send() )
				{
				
					# Load Thank You Message
					$this->load->view('contact_submitted');
				}
				
				# Load Footer Template
				$this->load->view('templates/footer');
				
			}
		}
	}

?>