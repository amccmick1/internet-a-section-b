<?php

	class Admin extends CI_Controller {
	
		function __construct() {
		
			parent::__construct();
			
			# Load URL Helper base_url method for header
			$this->load->helper('url');
			
			# Load form helper for dynamic form building
			$this->load->helper('form');
		
		}
	
		function index() {
			
			if ($this->session->userdata('email') == 'amccmick1@googlemail.com') {
			
				# Populate Read table
				# Load login Model
				$this->load->model('register_model');
				$users = new Register_Model();
			
				# Get all Users
				$data = array();
				if ($query = $users->show_all_users())
				{
					$data['records'] = $query;
				}

				# Load Admin Page
				$this->load->view('templates/header');
				$this->load->view('templates/nav');
				$this->load->view('admin', $data);
				$this->load->view('templates/footer');
				
			}
			else {
				
				# Load Login Page
				$this->load->view('templates/header');
				$this->load->view('templates/nav');
				$this->load->view('register');
				$this->load->view('templates/footer');
				
			}
		}
		
		function create() {
			
			# Fetch Post Data
			$email = $this->input->post('email');
			$password = $this->input->post('password');
		
			# Load login Model and Pass Post Data
			$this->load->model('register_model');
			$users = new Register_Model($email , $password);
			
			# Check Email Exists
			if ($users->check_if_email_exists() == false) {
			
				# Create new User
				$users->create_user();
			
			}
			
			$this->index();
		}
		
		function remove() {
		
			# Load login Model 
			$this->load->model('register_model');
			$users = new Register_Model();
			$users->remove_user();
			$this->index();
		
		}
	}
?>