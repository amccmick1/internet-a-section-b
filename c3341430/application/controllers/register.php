<?php

	class Register extends CI_Controller {
	
		function __construct() {
				
			parent::__construct();
						
			# Load Helper URL for Loading Views
			$this->load->helper('url');
			
			# Load Form helper for dynamic php form building
			$this->load->helper('form');
			
			# Include Form Validation Library
			$this->load->library('form_validation');
			
			# Add Validation Rules to Form
			$this->form_validation->set_rules('email','Email','strip_tags|required|trim|valid_email|xss_clean');
			$this->form_validation->set_rules('password','Password', 'strip_tags|required|xss_clean');
		
		}
	
		public function form() {

			# Load Header Template
			$this->load->view('templates/header');
			
			# Load Nav Template
			$this->load->view('templates/nav');
			
			# Load Contact Form
			$this->load->view('register');
			
			# Load Footer Template
			$this->load->view('templates/footer');
		}
		
		public function view_account() {
		
			# Load Email Address from Session
			$data['email'] = $this->session->userdata('email');
		
			# Load Header Template
			$this->load->view('templates/header');
				
			# Load Nav Template
			$this->load->view('templates/nav', $data);
				
			# Load Account Page and Pass Email data
			$this->load->view('account', $data);
				
			# Load Footer Template
			$this->load->view('templates/footer');
		
		}
		
		public function login_user() {
			
			# Fetch Post Data
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			
			# Load login Model and Pass Post Data
			$this->load->model('register_model');
			$login = new Register_Model($email , $password);
			
			if ($login->check_if_email_exists() == true) {
				
				# User Already Exists - Login
				if ($login->verify_user() == true ) {
				
					# Set User Email Address Session Data
					$this->session->set_userdata('email', $email);
					
					# Load Account Page
					$this->view_account();
				
				}
				else {
				
					# Incorrect Credentials
					$this->form();
				
				}
				
			}
			else {
				
				# login New User
			
				if ($query = $login->create_user()) {
					
					# Create New User Success
					
					# Set User Email Address Session Data
					$this->session->set_userdata('email', $email);

					# Load Account Page
					$this->view_account();
					
				}
				else {
					
					# Create New User Fail
					
				}
			}
		}
		
		public function account() {
			
			# Load Contact Form and Validate
			if ( $this->form_validation->run() == FALSE )
				$this->form();
			else {
				
				# Login User
				$this->login_user();
			}
		}
		
		public function logout() {
		
			# Remove Email Address from Session
			$this->session->set_userdata('email', null);
			$this->form();
		
		}
	}

?>