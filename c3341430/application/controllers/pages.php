<?php

	class Pages extends CI_Controller {
	
		# Fetch Page Views (Default = Home)
		function view( $page = 'home' ) {
		
			# Disallow Form and Account Pages
			$disallowed = array(
				'register',
				'contact',
				'account',
				'admin'
			);
			
			for ($i=0; $i < sizeOf($disallowed); $i++) {
			
				if ($page == $disallowed[$i])
					$page = 'disallowed';
			}
		
			# If File Doesn't Exist then Call show_404 Codeigniter function
			if ( !file_exists('application/views/'.$page.'.php') || $page == 'disallowed')
				show_404();
			
			# Store Website Title in Data Array
			$data['title'] = "Internet A - Section B";			
			
			# Load URL Helper to call Codeigniter base_url (get) method
			# Base URL value is stored in config/config.php
			$this->load->helper('url');
			
			# Load Header Template + Pass Website Title to Header
			$this->load->view('templates/header', $data);
			
			# Load Nav Template
			$this->load->view('templates/nav');
			
			# Load Page Content
			$this->load->view($page);
			
			# Load Footer Template
			$this->load->view('templates/footer');
		
		}
	
	}

?>