<?php

	class Register_Model extends CI_Model {
	
		private $email;
		private $password;
		
		function __construct($email = null, $password = null) {
		
			parent::__construct();
			$this->email = $email;
			$this->password = $password;
		}
	
		public function verify_user() {
		
			$this->db->where('email', $this->email);
			$this->db->where('password', sha1($this->password));
			$query = $this->db->get('users');
			
			if ( $query->num_rows == 1 )
				return true;		
		}
		
		public function create_user() {
			
			$new_user_insert_data = array(
				'email' => $this->email,
				'password' => sha1($this->password)
			);
		
			$insert = $this->db->insert('users', $new_user_insert_data);
			return $insert;
		}
	
		public function check_if_email_exists() {
		
			$this->db->where('email', $this->email);
			$result = $this->db->get('users');
			
			if ( $result->num_rows() > 0)
				return true;
		}
	
		public function remove_user() {
		
			$this->db->where('password', $this->uri->segment(3));
			$this->db->delete('users');
			
		}
		
		public function show_all_users() {
		
			$query = $this->db->get('users');
			return $query->result();
		
		}
	}

?>