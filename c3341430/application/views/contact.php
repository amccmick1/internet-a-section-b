<section id="contact" class="main style3 secondary">
	<div class="content container">
		<header>
			<h2>Say Hello.</h2>
		</header>
		<div class="box container small">
			<?php 
				echo validation_errors();
				echo form_open('contact/submit');			
			?>
				<div class="row half">
					<div class="6u">
						<?= form_input(array('name' => 'name', 'placeholder' => 'Name', 'value' => set_value('name'))); ?>
					</div>
					<div class="6u">
						<?= form_input(array('name' => 'email', 'placeholder' => 'Email', 'value' => set_value('email'))); ?>
					</div>
				</div>
				<div class="row half">
					<div class="12u">
						<?= form_textarea(array('id' => 'counttextarea', 'name' => 'message', 'placeholder' => 'Message', 'rows' => 6, 'value' => set_value('message'))); ?>
					</div>
					<p><span id="countchars"></span> Characters Remaining</p>
				</div>
				<div class="row">
					<div class="12u">
						<ul class="actions">
							<li>
								<?= form_submit('submit', 'Send Message'); ?>
							</li>
						</ul>
					</div>
				</div>
			<?= form_close(); ?>	
		</div>
	</div>
</section>