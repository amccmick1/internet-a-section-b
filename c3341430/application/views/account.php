<section id="register" class="main style3 primary">
	<div class="content container">		
		<header>
			<h2>Account</h2>
			<p>Welcome to Internet A Section A: <?= $email; ?></p>
			<br />
			<?= form_open('register/logout'); ?>
				<div class="row">
					<div class="12u">
						<ul class="actions">
							<li><?= form_submit('submit', 'Logout'); ?></li>
						</ul>
					</div>
				</div>
			<?= form_close(); ?>
		</header>
	</div>
</section>