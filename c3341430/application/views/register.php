<section id="register" class="main style3 primary">
	<div class="content container">
		<header>
			<h2>Register / Login</h2>
			<p>Enter your details here to register or, if you're an existing user, login to the site. Registration is free. Once registered, you will be able to login to the site here. Be sure to remember your email address and password for logon access to the site.</p>
			<br />
			<?php 
				echo validation_errors();
				echo form_open('register/account');			
			?>
				<div class="row half">
					<div class="6u">
						<?= form_input(array('name' => 'email', 'placeholder' => 'Email', 'value' => set_value('email'))); ?>
					</div>
					<div class="6u">
						<?= form_input(array('name' => 'password', 'placeholder' => 'Password', 'title' => 'At least eight characters containing at least one number, one lower, and one upper letter' ,'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}', 'type' =>'password')); ?>
					</div>
				</div>
				<div class="row">
						<div class="12u">
						<ul class="actions">
							<li><?= form_submit('submit', 'Submit'); ?></li>
						</ul>
					</div>
				</div>
			<?= form_close(); ?>	
		</header>
	</div>
</section>