<section id="one" class="main style2 right dark fullscreen">
	<div class="content box style2">
		<header>
			<h2>About</h2>
		</header>
		<p>This website has been created in partial fulfilment of a BSC(Hons) Computing course
		at Leeds Beckett University. The site 'Section B' is a html content and assets clone of 'Section A' but implemented and modified for use as a MVC application using the Codeigniter Framework</p> 
	</div>
</section>
