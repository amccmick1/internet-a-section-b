<section id="register" class="main style3 primary">
	<div class="content container">
		<header>
			<h2>Create</h2>
			<br/>
			<?= form_open('admin/create'); ?>
				<div class="row half">
					<div class="6u">
						<?= form_input(array('name' => 'email', 'placeholder' => 'Email', 'value' => set_value('email'))); ?>
					</div>
					<div class="6u">
						<?= form_input(array('name' => 'password', 'placeholder' => 'Password', 'title' => 'At least eight characters containing at least one number, one lower, and one upper letter' ,'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}', 'type' =>'password')); ?>
					</div>
				</div>
				<div class="row">
						<div class="12u">
						<ul class="actions">
							<li><?= form_submit('submit', 'Submit'); ?></li>
						</ul>
					</div>
				</div>
			<?= form_close(); ?>
		</header>
		<hr/>
		<header>
			<br/>
			<h2>View</h2>
			<br/>
			<?php if(isset($records)): foreach($records as $row): ?>
				<div class="row half">
					<div class="6u">
						<h2><?= anchor("admin/remove/$row->password", $row->email); ?></h2>
					</div>
					<div class="6u">
						<p><?= $row->password; ?></p>
					</div>
				</div>
			<?php endforeach; ?>
			<?php else : ?>
				<h3>No records were returned.</h3>
			<?php endif; ?>
		</header>
		<hr/>
		<header>
			<br/>
			<h2>Remove</h2>
			<br/>
			<p>To remove an account simply click on the email address in the view section.</p>
		</header>
	</div>
</section>