<!DOCTYPE HTML>
<head>
	<title>Internet A - Section B</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!--[if lte IE 8]><script src="<?= base_url(); ?>css/ie/html5shiv.js"></script><![endif]-->
	<script src="<?= base_url(); ?>js/jquery.min.js"></script>
	<script src="<?= base_url(); ?>js/jquery.poptrox.min.js"></script>
	<script src="<?= base_url(); ?>js/jquery.scrolly.min.js"></script>
	<script src="<?= base_url(); ?>js/jquery.scrollgress.min.js"></script>
	<script src="<?= base_url(); ?>js/skel.min.js"></script>
	<script src="<?= base_url(); ?>js/init.js"></script>
	<!-- Removed noscript due to base_url get method not being called inside these tags
	<noscript> -->
		<!-- <link rel="stylesheet" href="<?= base_url(); ?>css/skel.css" /> -->
		<link rel="stylesheet" href="<?= base_url(); ?>css/style.css" />
		<link rel="stylesheet" href="<?= base_url(); ?>css/style-wide.css" />
		<link rel="stylesheet" href="<?= base_url(); ?>css/style-normal.css" />
	<!-- </noscript> -->
	<!--[if lte IE 8]><link rel="stylesheet" href="<?= base_url(); ?>css/ie/v8.css" /><![endif]-->
	<script src="<?= base_url(); ?>js/char.count.live.js"></script>
	<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>-->
	<script src="<?= base_url(); ?>js/map.js"></script>
</head>
<body>